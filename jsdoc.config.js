'use strict'

module.exports = {
  plugins: [
    'node_modules/jsdoc-vuejs',
    'plugins/markdown'
  ],
  recurseDepth: 10,
  source: {
    includePattern: '\\.(vue|js)$',
    excludePattern: '(^|\\/|\\\\)_'
  },
  sourceType: 'module',
  tags: {
    allowUnknownTags: true,
    dictionaries: ['jsdoc', 'closure']
  },
  templates: {
    cleverLinks: false,
    monospaceLinks: true
  }
}
