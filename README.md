# Portfolio
A small web application presenting projects, skills, arbitrary portfolio content.

## Project setup

#### Local Dev Certificate
The frontend development server is configured to serve `https` via a custom local certificate `assets/cert.pem`
and corresponding key `assets/key.pem`.

You may generate and install these via [mkcert](https://github.com/FiloSottile/mkcert).
After following the [installation instructions](https://github.com/FiloSottile/mkcert#installation),
navigate to `assets/` and run

```bash
mkcert -install
mkcert localhost 127.0.0.1 ::1
```

#### Install and Run

Now, you may run the installation of the actual web app dependencies (listed in `package.json`) via

```
npm install
```

Compile, serve and hot-reload for development via
```
npm run serve
```

or compile and minify for production via
```
npm run build
```

`babel-jest` dependency currently needs to stay at outdated version `24.9.0` (or lower) due to [an issue with
mocking imports](https://github.com/facebook/jest/issues/10147).

#### License Report
The legal notice page is able to display a list of used npm packages assembled by `license-report`.

Run
```bash
npm run license
``` 
to generate the respective JSON file after installing this project's dependencies.
