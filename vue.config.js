const fs = require('fs')
module.exports = {
  publicPath: '/',
  lintOnSave: process.env.NODE_ENV !== 'production',
  devServer: {
    https: true,
    key: fs.existsSync('assets/key.pem') ? fs.readFileSync('assets/key.pem') : 'DUMMYKEY',
    cert: fs.existsSync('assets/cert.pem') ? fs.readFileSync('assets/cert.pem') : 'DUMMYCERT'
  },
  pages: {
    landing: {
      // entry for the page
      entry: './src/pages/landing/main.js',
      // the source template
      template: 'src/pages/index.html',
      filename: 'index.html',
      // when using title option,
      // template title tag needs to be
      // <title><%= htmlWebpackPlugin.options.title %></title>
      title: 'Portfolio',
      chunks: ['chunk-vendors', 'chunk-common', 'landing']
    },
    legal: {
      entry: './src/pages/legal/main.js',
      template: 'src/pages/index.html',
      filename: 'legal.html',
      title: 'Legal Notes',
      chunks: ['chunk-vendors', 'chunk-common', 'legal']
    }
  }
}
