import { hexToRgba, parseLicenseJSON, skillsToChartData, cleanString } from '@/core/utils.js'

describe('utils', () => {
  it('hexToRgba computes correctly', () => {
    const hex = '#3f51b5'
    let expectedRgba = 'rgba(63,81,181,1)'
    let computedRgba = hexToRgba(hex)
    expect(computedRgba).toBe(expectedRgba)

    // now with passed alpha value
    expectedRgba = 'rgba(63,81,181,0.123)'
    computedRgba = hexToRgba(hex, 0.123)
    expect(computedRgba).toBe(expectedRgba)
  })

  it('license report parsed correctly', () => {
    const repJSON = [
      {
        department: 'g',
        relatedTo: 's',
        name: '@abc/js',
        licensePeriod: 'perpetual',
        material: 'm',
        licenseType: 'Apache-2.0',
        link: 'https://www.lol.lol',
        comment: '5.6.55'
      },
      {
        department: 'g',
        relatedTo: 's',
        name: '@abc/js1',
        licensePeriod: 'perpetual',
        material: 'm',
        licenseType: 'MIT',
        link: 'https://www.lol.lol',
        comment: '5.6.55'
      }
    ]

    const parsed = parseLicenseJSON(repJSON, 'type', 'item', 'lic',
      'other', 'emptyStuff')
    const expected = [
      { type: 'source code/npm pkg', item: '@abc/js', lic: 'Apache-2.0', other: '', emptyStuff: '' },
      { type: 'source code/npm pkg', item: '@abc/js1', lic: 'MIT', other: '', emptyStuff: '' }
    ]
    expect(parsed).toEqual(expected)
  })

  it('skillsToChartData', () => {
    const skills = [
      {
        name: 'skill0',
        abbr: 'skill0',
        icon: 'mdi-skill0',
        category: 'cat0',
        have: 5,
        want: 0
      },
      {
        name: 'skill1',
        abbr: 's1',
        icon: 'mdi-skill1',
        category: 'cat1',
        have: 2.5,
        want: 1.5
      },
      {
        name: 'skill2',
        abbr: 's2',
        icon: 'mdi-skill2',
        category: 'cat0',
        have: 1.5,
        want: 2
      }
    ]
    // expected: two blobs: one for haves, one for wants; each three axis, sorted
    const expectedCD = [
            // haves
            [
              { axis: 'skill0', value: 5 },
              { axis: 'skill2', value: 1.5 },
              { axis: 'skill1', value: 2.5 }
            ],
            // wants
            [
              { axis: 'skill0', value: 0 },
              { axis: 'skill2', value: 2 },
              { axis: 'skill1', value: 1.5 }
            ]
    ]

    const expectedCDAbbr = [
            // haves
            [
              { axis: 'skill0', value: 5 },
              { axis: 's2', value: 1.5 },
              { axis: 's1', value: 2.5 }
            ],
            // wants
            [
              { axis: 'skill0', value: 0 },
              { axis: 's2', value: 2 },
              { axis: 's1', value: 1.5 }
            ]
    ]

    expect(skillsToChartData(skills)).toEqual(expectedCD)
    expect(skillsToChartData(skills, true)).toEqual(expectedCDAbbr)
  })

  it('cleaning string', () => {
    const strings = [
      ['nothingwronghere1', 'nothingwronghere1'],
      ['NoThInGwRoNgHeRe2', 'NoThInGwRoNgHeRe2'],
      ['a.bit+problematic', 'a_bit_problematic'],
      ['T/h\\i.s1_too!?', 'T_h_i_s1_too__'],
      ['C++', 'C__']
    ]

    for (const pair of strings) {
      expect(cleanString(pair[0])).toBe(pair[1])
    }
  })
})
