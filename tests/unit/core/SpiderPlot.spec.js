import Vue from 'vue'
import Vuetify from 'vuetify'

import { mount, createLocalVue } from '@vue/test-utils'

import SpiderPlot from '@/core/components/SpiderPlot'

Vue.use(Vuetify)

describe('Core SpiderPlot', () => {
  const errorHandler = (err, vm, info) => { expect(err).toBeInstanceOf(Error) }
  const localVue = createLocalVue({ errorHandler })

  const chartData = [
          [{ axis: 'one', value: 1 }, { axis: 'one', value: 2 }, { axis: 'two', value: 3 }],
          [{ axis: 'one', value: 2 }]
  ]
  const mockWHeight = 1234
  const mockWWidth = 2456
  const mockContainerWidth = 1228

  let wrapper
  beforeEach(() => {
    const vuetify = new Vuetify()
    window.innerHeight = mockWHeight
    window.innerWidth = mockWWidth
    wrapper = mount(
      SpiderPlot,
      {
        localVue,
        vuetify,
        propsData: {
          isActive: true,
          chartData: chartData
        }
      }
    )
  })

  it('sizes correctly', async () => {
    expect(window.innerHeight).toBe(mockWHeight)
    jest.spyOn(wrapper.vm.$refs.spiderPlot, 'clientWidth', 'get').mockReturnValue(mockContainerWidth)
    wrapper.vm.onResize()
    expect(wrapper.vm.containerWidth).toBe(mockContainerWidth)
    expect(wrapper.vm.spiderPlotOptions.w).toBe(
            mockContainerWidth - wrapper.vm.margin.all - wrapper.vm.margin.left - wrapper.vm.margin.right)
    expect(wrapper.vm.spiderPlotOptions.w).toBe(wrapper.vm.spiderPlotOptions.w)
  })
})
