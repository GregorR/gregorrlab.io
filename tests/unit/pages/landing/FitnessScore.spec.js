import Vue from 'vue'
import Vuetify from 'vuetify'

import VueKatex from 'vue-katex'
import { mount, createLocalVue } from '@vue/test-utils'

import FitnessScore from '@/pages/landing/components/FitnessScore'

Vue.use(Vuetify)
Vue.use(VueKatex)
const localVue = createLocalVue()

describe('Landing FitnessScore', () => {
  let wrapper
  beforeEach(() => {
    const vuetify = new Vuetify()
    wrapper = mount(
      FitnessScore,
      {
        localVue,
        vuetify,
        propsData: {
          maxScore: 5
        }
      }
    )
  })

  it('compute: mixed values', async () => {
    await wrapper.setProps({ provisions: [1, 2, 3, 5, 5], desires: [2, 4, 1, 4, 0] })
    // Delta = (2*(-1) + 4*(-2) + 2 + 4 + 0)/11 = -4/11
    expect(wrapper.vm.delta).toBe(-4 / 11)
    // s = (5 - (4/11))/5 = 1 - 4/55 = 0.92727
    expect(wrapper.vm.score).toBe(1 - 4 / 55)
  })

  it('compute: extreme values', async () => {
    await wrapper.setProps({ provisions: [0.5, 1, 1.5], desires: [0, 0, 0] })
    expect(wrapper.vm.delta).toBe(null)
    expect(wrapper.vm.score).toBe(null)

    await wrapper.setProps({ provisions: [0.5, 1, 1.5], desires: [0, 1, 0] })
    expect(wrapper.vm.delta).toBe(0)
    expect(wrapper.vm.score).toBe(1)

    await wrapper.setProps({ provisions: [1, 5, 5], desires: [0, 5, 5] })
    expect(wrapper.vm.delta).toBe(0)
    expect(wrapper.vm.score).toBe(1)

    await wrapper.setProps({ provisions: [0], desires: [4] })
    expect(wrapper.vm.delta).toBe(-4)
    expect(wrapper.vm.score).toBeCloseTo(0.2, 5)

    await wrapper.setProps({ provisions: [0], desires: [5] })
    expect(wrapper.vm.delta).toBe(-5)
    expect(wrapper.vm.score).toBeCloseTo(0, 5)

    await wrapper.setProps({ provisions: [0, 0.5, 1, 1.5], desires: [5, 5, 5, 5] })
    // Delta = 5/20 * {(-5) + (-4.5) + (-4) + (-3.5)} = 5 * {-17} = -85/20 = -4 1/4 = -4.25
    expect(wrapper.vm.delta).toBe(-4.25)
    // s = 1 - 4.25/5
    expect(wrapper.vm.score).toBe(1 - 4.25 / 5)
  })
})
