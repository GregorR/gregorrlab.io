/* eslint-disable */
const mockData = {
  data: {
     maxRating: 5,
     defaultItemProps: {
       name: { label: 'Skill Name', type: 'textfield' },
       category: { label: 'Category', type: 'textfield' },
       have: {
         label: 'My Skill Level (prob. 0)',
         type: 'dropdown',
         items: [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5],
         color: 'secondary'
       },
       want: {
         label: 'Min. Level You Target',
         type: 'dropdown',
         items: [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5],
         color: 'accent'
       }
     },
     defaultItem: {
       name: '',
       abbr: '',
       icon: 'mdi-face-shimmer',
       category: 'uncategorized',
       have: 0,
       want: 0
     },
     headers: [
       { text: 'Skill', value: 'name', align: 'left', groupable: false },
       { text: 'Category', value: 'category', align: 'center' },
       { text: 'Have', value: 'have', align: 'right', groupable: false },
       { text: 'Desired', value: 'want', align: 'right', groupable: false }
     ],
     skills: [
       {
         name: 'mockPython',
         abbr: 'python',
         icon: 'mdi-language-python',
         category: 'Programming Languages',
         have: 5,
         want: 0
       },
       {
         name: 'mockC++',
         abbr: 'C++',
         icon: 'mdi-language-cpp',
         category: 'Programming Languages',
         have: 2.5,
         want: 0
       },
       {
         name: 'mockJavascript',
         abbr: 'JS',
         icon: 'mdi-language-javascript',
         category: 'Programming Languages',
         have: 3,
         want: 0
       }
     ]
   }
}

jest.mock('@/base/skillTableStorage', () => ({
  __esModule: true,
  default: mockData
})
)
