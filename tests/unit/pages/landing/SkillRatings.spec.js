import Vue from 'vue'
import Vuetify from 'vuetify'

import './store.mock'
import { mount, createLocalVue } from '@vue/test-utils'
import ratingsStore from '@/base/skillTableStorage'
import { cleanString } from '@/core/utils'
import SkillRatings from '@/pages/landing/components/SkillRatings'

Vue.use(Vuetify)
const localVue = createLocalVue()
describe('Landing SkillRatings', () => {
  let wrapper

  beforeEach(() => {
    // could not find a good way to reset the import mock, but tests don't interfere anyway.
    const vuetify = new Vuetify()
    wrapper = mount(
      SkillRatings,
      {
        localVue,
        vuetify
      }
    )
  })

  it('v-rating reads correctly from store', async () => {
    // deep copy since other tests might alter it
    const localStore = JSON.parse(JSON.stringify(ratingsStore.data))
    // currently can't find a better way to set prop on v-data-table s.t. all items are displayed per page
    jest.spyOn(console, 'error').mockImplementation(() => {})
    wrapper.vm.$refs.dataTable._props.itemsPerPage = -1
    await wrapper.vm.$nextTick()
    for (const skill of localStore.skills) {
      let rating = wrapper.get(('#trHave' + cleanString(skill.name)))
      let halves = 0.5 * rating.findAll('.mdi-circle-half-full').length
      let active = rating.findAll('.mdi-circle').length + halves
      let inactive = rating.findAll('.mdi-circle-outline').length + halves

      expect(active).toBe(skill.have)
      expect(active + inactive).toBe(localStore.maxRating)

      rating = wrapper.get('#trWant' + cleanString(skill.name))
      halves = 0.5 * rating.findAll('.mdi-circle-half-full').length
      active = rating.findAll('.mdi-circle').length + halves
      inactive = rating.findAll('.mdi-circle-outline').length + halves

      expect(active).toBe(skill.want)
      expect(active + inactive).toBe(localStore.maxRating)
    }
  })

  it('clicked v-rating correctly saves to store', async () => {
    const skill = ratingsStore.data.skills[0]
    const rating = wrapper.get('#trWant' + cleanString(skill.name))
    expect(ratingsStore.data.skills[0].want).toBe(0)
    const thrdButton = rating.findAll('button').at(2)
    await thrdButton.trigger('click')
    expect(ratingsStore.data.skills[0].want).toBe(3)
  })

  it('add item correctly saves to store', async () => {
    jest.spyOn(console, 'warn').mockImplementation(() => {})
    await wrapper.get('#addItem').trigger('click')
    expect(wrapper.vm.editedItem.have).toBe(0)
    expect(wrapper.vm.editedItem.want).toBe(0)

    const nameVal = 'FuNnYSkiLlLoL'
    const catVal = 'FuNnYCaT'
    const haveVal = 1.5
    const wantVal = 2.5
    await wrapper.get('#addItemname').setValue(nameVal)
    await wrapper.get('#addItemcategory').setValue(catVal)
    await wrapper.get('#addItemhave').setValue(haveVal)
    await wrapper.get('#addItemwant').setValue(wantVal)
    // check local sets
    expect(wrapper.vm.editedItem.name).toBe(nameVal)
    expect(wrapper.vm.editedItem.category).toBe(catVal)
    expect(Number(wrapper.vm.editedItem.have)).toBe(haveVal)
    expect(Number(wrapper.vm.editedItem.want)).toBe(wantVal)

    await wrapper.get('#addItemSave').trigger('click')
    // check resets
    expect(wrapper.vm.editedItem.name).toBe(ratingsStore.data.defaultItem.name)
    expect(wrapper.vm.editedItem.abbr).toBe(ratingsStore.data.defaultItem.abbr)
    expect(wrapper.vm.editedItem.category).toBe(ratingsStore.data.defaultItem.category)
    expect(wrapper.vm.editedItem.have).toBe(ratingsStore.data.defaultItem.have)
    expect(wrapper.vm.editedItem.want).toBe(ratingsStore.data.defaultItem.want)

    // check global sets
    const expected = {
      name: nameVal,
      abbr: nameVal,
      icon: 'mdi-face-shimmer',
      category: catVal,
      have: haveVal,
      want: wantVal
    }
    expect(ratingsStore.data.skills[ratingsStore.data.skills.length - 1]).toEqual(expected)
  })

  it('edit item correctly saves to store', async () => {
    jest.spyOn(console, 'error').mockImplementation(() => {})
    await wrapper.get('#addItem').trigger('click')
    await wrapper.get('#addItemSave').trigger('click')
    wrapper.vm.$refs.dataTable._props.itemsPerPage = -1
    await wrapper.vm.$nextTick()
    const editItem = ratingsStore.data.skills[2]
    await wrapper.get('#editItem' + cleanString(editItem.name)).trigger('click', { item: editItem })

    expect(wrapper.vm.editedItem.name).toBe(editItem.name)
    expect(wrapper.vm.editedItem.category).toBe(editItem.category)
    expect(wrapper.vm.editedItem.have).toBe(editItem.have)
    expect(wrapper.vm.editedItem.want).toBe(editItem.want)
    expect(wrapper.vm.dialog).toBeTruthy()
  })
})
