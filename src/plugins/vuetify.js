
import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import minifyTheme from 'minify-css-string'

Vue.use(Vuetify)

const opts = {
  minifyTheme,
  theme: {
    themes: {
      dark: {
        primary: '#3f51b5',
        secondary: '#2ba679',
        info: '#000000',
        text: '#ffffff',
        sectionBg: '#707072',
        barBg: '#707072',
        objectBg: '#0a0f12',
        accent: '#a62b42',
        success: '#70ad47',
        error: '#b71c1c'
      },
      light: {
        primary: '#0173b1',
        secondary: '#2ba679',
        // info: '#2ba679',
        info: '#0173b1',
        text: '#000000',
        sectionBg: '#ffffff',
        barBg: '#707072',
        objectBg: '#cacaca',
        accent: '#a62b42',
        success: '#70ad47',
        error: '#b71c1c'
      }
    },
    dark: false,
    options: {
      customProperties: true
    }
  },
  themeCache: {
    get: key => localStorage.getItem(key),
    set: (key, value) => localStorage.setItem(key, value)
  }
}

export default new Vuetify(opts)
