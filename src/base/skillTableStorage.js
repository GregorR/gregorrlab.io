/**
 * Shared Storage for Data in Skill Ratings Table.
 * Shares data between parent and child component.
 */
export default {

  data: {
    maxRating: 5,
    defaultItemProps: {
      name: { label: 'Skill Name', type: 'textfield' },
      category: { label: 'Category', type: 'textfield' },
      have: { label: 'My Skill Level (prob. 0)', type: 'dropdown', items: [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5], color: 'secondary' },
      want: { label: 'Min. Level You Target', type: 'dropdown', items: [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5], color: 'accent' }
    },
    defaultItem: {
      name: '',
      abbr: '',
      icon: 'mdi-face-shimmer',
      category: '#uncategorized',
      have: 0,
      want: 0
    },
    headers: [
      { text: 'Skill', value: 'name', align: 'left', groupable: false },
      { text: 'Category', value: 'category', align: 'center' },
      { text: 'Have', value: 'have', align: 'right', groupable: false },
      { text: 'Desired', value: 'want', align: 'right', groupable: false }
    ],
    skills: [
      {
        name: 'python',
        abbr: 'python',
        icon: 'mdi-language-python',
        category: 'Programming Languages',
        have: 5,
        want: 0
      },
      {
        name: 'Javascript',
        abbr: 'JS',
        icon: 'mdi-language-javascript',
        category: 'Programming Languages',
        have: 3,
        want: 0
      },
      {
        name: 'Vue.js/HTML/CSS',
        abbr: 'Vue.js',
        icon: 'mdi-vuejs',
        category: 'Programming Languages',
        have: 3,
        want: 0
      },
      {
        name: 'LaTeX',
        abbr: 'LaTeX',
        icon: 'iconify-latex',
        category: 'Programming Languages',
        have: 4,
        want: 0
      },
      {
        name: 'Excel/VBA',
        abbr: 'Excel',
        icon: 'mdi-microsoft-excel',
        category: 'Programming Languages',
        have: 2,
        want: 0
      },
      {
        name: 'NumPy',
        abbr: 'NumPy',
        icon: 'iconify-numpy',
        category: 'Frameworks/Libraries',
        have: 4.5,
        want: 0
      },
      {
        name: 'pandas',
        abbr: 'pandas',
        icon: 'iconify-pandas',
        category: 'Frameworks/Libraries',
        have: 4.5,
        want: 0
      },
      {
        name: 'pyTorch',
        abbr: 'pyTorch',
        icon: 'iconify-pytorch',
        category: 'Frameworks/Libraries',
        have: 4.5,
        want: 0
      },
      {
        name: 'pytest',
        abbr: 'pytest',
        icon: 'mdi-check-bold',
        category: 'Frameworks/Libraries',
        have: 4.5,
        want: 0
      },
      {
        name: 'SQLAlchemy',
        abbr: 'SQLAlchemy',
        icon: 'mdi-database',
        category: 'Frameworks/Libraries',
        have: 3,
        want: 0
      },
      {
        name: 'matplotlib',
        abbr: 'matplotlib',
        icon: 'mdi-chart-scatter-plot-hexbin',
        category: 'Frameworks/Libraries',
        have: 3.5,
        want: 0
      },
      {
        name: 'Data Science',
        abbr: 'DS',
        icon: 'mdi-dots-hexagon',
        category: 'Topics',
        have: 4,
        want: 0
      },
      {
        name: 'Machine Learning',
        abbr: 'ML',
        icon: 'mdi-state-machine',
        category: 'Topics',
        have: 4.5,
        want: 0
      },
      {
        name: 'Computer Vision',
        abbr: 'CV',
        icon: 'mdi-eye-settings',
        category: 'Topics',
        have: 5,
        want: 0
      },
      {
        name: 'linux',
        abbr: 'linux',
        icon: 'mdi-linux',
        category: 'Tools/OSs',
        have: 3.5,
        want: 0
      },
      {
        name: 'MS Windows',
        abbr: 'Windows',
        icon: 'mdi-microsoft-windows',
        category: 'Tools/OSs',
        have: 4,
        want: 0
      },
      {
        name: 'MS Powerpoint',
        abbr: 'Powerpoint',
        icon: 'mdi-microsoft-powerpoint',
        category: 'Tools/OSs',
        have: 4.5,
        want: 0
      },
      {
        name: 'git',
        abbr: 'git',
        icon: 'mdi-git',
        category: 'Tools/OSs',
        have: 4.0,
        want: 0
      },
      {
        name: 'containerization',
        abbr: 'docker',
        icon: 'mdi-docker',
        category: 'Tools/OSs',
        have: 3.0,
        want: 0
      },
      {
        name: 'kubernetes',
        abbr: 'k8s',
        icon: 'mdi-kubernetes',
        category: 'Tools/OSs',
        have: 4.0,
        want: 0
      }

    ]
  },

  fetchData () {
  }

}
