/**
 * @module Utils
 */

/**
 * Convert a color in hexadecimal to rgba
 *
 * @memberof module:Utils
 *
 * @param hex {string} - color code in hexadecimal that should be converted to rgba.
 * @param [alpha=1.0] {number} - alpha value to apply after conversion. needs to be 0.-1.
 * @returns {string} - color in rgb with alpha.
 */
export function hexToRgba (hex, alpha = 1.0) {
  hex = hex.replace('#', '')

  if (hex.length === 3) {
    hex = `${hex[0]}${hex[0]}${hex[1]}${hex[1]}${hex[2]}${hex[2]}`
  }

  const r = parseInt(hex.substring(0, 2), 16)
  const g = parseInt(hex.substring(2, 4), 16)
  const b = parseInt(hex.substring(4, 6), 16)
  return `rgba(${r},${g},${b},${alpha})`
}

/**
 * Format a license report json object generated with npm plugin `license-report` s. t. it matches
 * row format in the designated target table.
 * The `aliases` are the column value keys. Type column will receive value 'source code/npm pkg'.
 *
 * @memberof module:Utils
 *
 * @param json {JSON} - The object to be parsed, holds all info.
 * @param typeAlias {String}
 * @param nameAlias {String}
 * @param licenseAlias {String}
 * @param args {Array} - further column keys that will receive empty Strings as values.
 * @returns {Array} - array of objects, each object represents a row.
 */
export function parseLicenseJSON (json, typeAlias, nameAlias, licenseAlias, ...args) {
  const parsed = []
  for (const dependency of json) {
    const parsedItem = {}
    parsedItem[typeAlias] = 'source code/npm pkg'
    parsedItem[nameAlias] = dependency.name
    parsedItem[licenseAlias] = dependency.licenseType
    for (const otherKey of args) { parsedItem[otherKey] = '' }
    parsed.push(parsedItem)
  }
  return parsed
}

/**
 * Parse skills from storage into chart-compatible format. TODO Also sort by category.
 *
 * @param skills {Array}
 * @param abbreviate {Boolean} - if true, use key `abbr` instead of `name` for name.
 *
 * @return {Array} - array of two arrays of objects, one array for haves, one for wants. each object has
 * { axis: <axisname>, value: <value> }
 */
export function skillsToChartData (skills, abbreviate = false) {
  const chartData = []
  skills = [...skills].sort((s1, s2) => { return s1.category.localeCompare(s2.category) })
  const nameKey = abbreviate ? 'abbr' : 'name'
  chartData.push(skills.map((obj) => { return { axis: obj[nameKey], value: obj.have } }))
  chartData.push(skills.map((obj) => { return { axis: obj[nameKey], value: obj.want } }))
  return chartData
}

/**
 * Convert a given string to a string eligible as CSS selector. Replace every character that is not a letter or
 * literal by an underscore.
 * @param string {String} - input string possibly containing invalid characters
 */
export function cleanString (string) {
  return string.replace(/[^a-z0-9]/gi, '_')
}
