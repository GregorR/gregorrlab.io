
// Adapted from The Radar Chart Function by Nadieh Bremer (VisualCinnamon.com), which themselves were
// inspired by the code of alangrafu (MIT License)

const d3 = Object.assign({}, require('d3'), require('d3-scale'))

export default function spiderPlot (id, data, options) {
  // default config
  const cfg = {
    w: 600, // Width of the outermost circle
    h: 600, // Height of the outermost circle
    margin: { top: 20, right: 20, bottom: 20, left: 20 }, // margins of the SVG
    levels: 3, // nr of levels or inner circles
    maxValue: 0, // What is the value that the biggest circle will represent
    valueFormat: '%', // format string passed to d3.format
    labelFactor: 1.25, // Distance between outer circle radius and legend labels
    labelFs: '12px', // label font-size (all axes)
    labelColor: '#000000', // label text color
    wrapWidth: 60, // The number of pixels after which a label needs to be given a new line
    opacityArea: 0.35, // opacity of the blob area
    dotRadius: 4, // size of the colored data point dots of each blob
    opacityCircles: 0.1, // opacity of the canvas circles of each blob
    strokeWidth: 1, // width of the stroke around each blob
    color: d3.scaleOrdinal(d3.schemeCategory10) // Color function
  }

  // override defaults if given
  if (typeof options !== 'undefined') {
    for (const oix in options) {
      if (typeof options[oix] !== 'undefined') {
        cfg[oix] = options[oix]
      }
    }
  }

  // If the supplied maxValue is smaller than the actual one, replace by the max in the data
  const maxValue = Math.max(cfg.maxValue, d3.max(data, function (i) {
    return d3.max(i.map(function (o) {
      return o.value
    }))
  }))

  const allAxis = (data[0].map(obj => { return obj.axis })) // Names of each axis
  const total = allAxis.length // The number of different axes
  var radius = Math.min(cfg.w / 2, cfg.h / 2) // Radius of the outermost circle
  var Format = d3.format(cfg.valueFormat) // Percentage formatting
  var angleSlice = Math.PI * 2 / total // The width in radians of each "slice"

  // Scale for the radius
  var rScale = d3.scaleLinear().range([0, radius]).domain([0, maxValue])

  /// //////////////////////////////////////////////////////
  /// ///////// Create the container SVG and g /////////////
  /// //////////////////////////////////////////////////////

  // Remove whatever chart with the same id/class was present before
  d3.select(id).select('svg').remove()

  // Initiate the spider chart SVG
  var svg = d3.select(id).append('svg')
          .attr('width', cfg.w + cfg.margin.left + cfg.margin.right)
          .attr('height', cfg.h + cfg.margin.top + cfg.margin.bottom)
          .attr('class', id.replace('#', ''))
  // Append a g element
  var g = svg.append('g').attr('transform',
          'translate(' + (cfg.w / 2 + cfg.margin.left) + ',' + (cfg.h / 2 + cfg.margin.top) + ')')

  /// //////////////////////////////////////////////////////
  /// /////// Glow filter for some extra pizzazz ///////////
  /// //////////////////////////////////////////////////////

  // Filter for the outside glow
  const filter = g.append('defs').append('filter').attr('id', 'glow')
  filter.append('feGaussianBlur').attr('stdDeviation', '10').attr('result', 'coloredBlur')
  const feMerge = filter.append('feMerge')
  feMerge.append('feMergeNode').attr('in', 'coloredBlur')
  feMerge.append('feMergeNode').attr('in', 'SourceGraphic')

  /// //////////////////////////////////////////////////////
  /// //////////// Draw the Circular grid //////////////////
  /// //////////////////////////////////////////////////////

  // Wrapper for the grid & axes
  const axisGrid = g.append('g').attr('class', 'axisWrapper')

  // Draw the background circles
  axisGrid.selectAll('.levels')
          .data(d3.range(1, (cfg.levels + 1)).reverse())
          .enter()
          .append('circle')
          .attr('class', 'gridCircle')
          .attr('r', (d) => { return radius * d / cfg.levels }) // d is data, one entry in the data object
          .style('fill', '#CDCDCD')
          .style('stroke', '#CDCDCD')
          .style('fill-opacity', cfg.opacityCircles)
          .style('filter', 'url(#glow)')

  // Text indicating at what value each level is
  axisGrid.selectAll('.axisLabel')
          .data(d3.range(1, (cfg.levels + 1)).reverse())
          .enter().append('text')
          .attr('class', 'axisLabel')
          .attr('x', 4)
          .attr('y', (d) => { return -d * radius / cfg.levels })
          .attr('dy', '0.4em')
          .style('font-size', cfg.labelFs)
          .style('color', cfg.labelColor)
          .text((d) => { return Format(maxValue * d / cfg.levels) })

  /// //////////////////////////////////////////////////////
  /// ///////////////// Draw the axes //////////////////////
  /// //////////////////////////////////////////////////////

  // abort calculation if empty data
  const dataLengths = []
  for (const series of data) { dataLengths.push(series.length) }
  if (dataLengths.every((el) => { return el === 0 })) {
    return
  }

  // Create the straight lines radiating outward from the center
  var axis = axisGrid.selectAll('.axis')
          .data(allAxis)
          .enter()
          .append('g')
          .attr('class', 'axis')
  // Append the lines
  axis.append('line')
          .attr('x1', 0)
          .attr('y1', 0)
          .attr('x2', function (d, i) {
            return rScale(maxValue * 1.1) * Math.cos(angleSlice * i - Math.PI / 2)
          })
          .attr('y2', function (d, i) {
            return rScale(maxValue * 1.1) * Math.sin(angleSlice * i - Math.PI / 2)
          })
          .attr('class', 'line')
          .style('stroke', 'white')
          .style('stroke-width', '2px')

  // Append the labels at each axis
  axis.append('text')
          .attr('class', 'legend')
          .style('font-size', cfg.labelFs)
          .style('color', cfg.labelColor)
          .attr('text-anchor', 'middle')
          .attr('dy', '0.35em')
          .attr('x', function (d, i) {
            return rScale(maxValue * cfg.labelFactor) * Math.cos(angleSlice * i - Math.PI / 2)
          })
          .attr('y', function (d, i) {
            return rScale(maxValue * cfg.labelFactor) * Math.sin(angleSlice * i - Math.PI / 2)
          })
          .text((d) => { return d })
          .call(wrap, cfg.wrapWidth)

  /// //////////////////////////////////////////////////////
  /// ////////// Draw the spider chart blobs ////////////////
  /// //////////////////////////////////////////////////////

  // The radial line function
  var blobLine = d3.lineRadial()
          .radius(function (d) { return rScale(d.value) })
          .angle(function (d, i) { return i * angleSlice })
          .curve(d3.curveCatmullRomClosed)
  // Create a wrapper for the blobs
  var blobWrapper = g.selectAll('.blobWrapper')
          .data(data)
          .enter().append('g')
          .attr('class', 'blobWrapper')

  // Append the backgrounds
  blobWrapper
          .append('path')
          .attr('class', 'blobArea')
          .attr('d', (d) => { return blobLine(d) })
          .style('fill', (d, i) => { return cfg.color(i) })
          .style('fill-opacity', cfg.opacityArea)
          .on('mouseover', function (d, i) {
            // Dim all blobs
            d3.selectAll('.blobArea')
                    .transition().duration(200)
                    .style('fill-opacity', 0.1)
            // Bring back the hovered-over blob
            d3.select(this)
                    .transition().duration(200)
                    .style('fill-opacity', 0.7)
          })
          .on('mouseout', function () {
            // Bring back all blobs
            d3.selectAll('.blobArea')
                    .transition().duration(200)
                    .style('fill-opacity', cfg.opacityArea)
          })

  // Create the outlines
  blobWrapper.append('path')
          .attr('class', 'blobStroke')
          .attr('d', (d) => { return blobLine(d) })
          .style('stroke-width', cfg.strokeWidth + 'px')
          .style('stroke', (d, i) => { return cfg.color(i) })
          .style('fill', 'none')
          .style('filter', 'url(#glow)')

  // Append the circles
  blobWrapper.selectAll('.blobCircle')
          .data((d, j) => { for (const ix in d) { d[ix].seriesIx = j }; return d })
          .enter().append('circle')
          .attr('class', 'blobCircle')
          .attr('r', cfg.dotRadius)
          .attr('cx', function (d, i) {
            return rScale(d.value) * Math.cos(angleSlice * i - Math.PI / 2)
          })
          .attr('cy', function (d, i) { // d is data, one entry in the data object
            return rScale(d.value) * Math.sin(angleSlice * i - Math.PI / 2)
          })
          .style('fill', (d) => { return cfg.color(d.seriesIx) })
          .style('fill-opacity', 0.8)

  /// //////////////////////////////////////////////////////
  /// ///// Append invisible circles for tooltip ///////////
  /// //////////////////////////////////////////////////////

  // Wrapper for the invisible circles on top
  var blobCircleWrapper = g.selectAll('.blobCircleWrapper')
          .data(data)
          .enter().append('g')
          .attr('class', 'blobCircleWrapper')

  // Append a set of invisible circles on top for the mouseover pop-up
  blobCircleWrapper.selectAll('.blobInvisibleCircle')
          .data((d) => { return d })
          .enter().append('circle')
          .attr('class', 'blobInvisibleCircle')
          .attr('r', cfg.dotRadius * 1.5)
          .attr('cx', (d, i) => { return rScale(d.value) * Math.cos(angleSlice * i - Math.PI / 2) })
          .attr('cy', (d, i) => { return rScale(d.value) * Math.sin(angleSlice * i - Math.PI / 2) })
          .style('fill', 'none')
          .style('pointer-events', 'all')
          .on('mouseover', function (d, i) {
            const newX = parseFloat(d3.select(this).attr('cx')) - 10
            const newY = parseFloat(d3.select(this).attr('cy')) - 10

            tooltip.attr('x', newX).attr('y', newY)
                    .text(Format(i.value))
                    .transition().duration(200)
                    .style('opacity', 1)
          })
          .on('mouseout', () => { tooltip.transition().duration(200).style('opacity', 0) })

  // Set up the small tooltip for when you hover over a circle
  const tooltip = g.append('text')
          .attr('class', 'tooltip')
          .style('font-size', cfg.labelFs)
          .style('opacity', 0)

  /// //////////////////////////////////////////////////////
  /// //////////////// Helper Function /////////////////////
  /// //////////////////////////////////////////////////////

  // Taken from http://bl.ocks.org/mbostock/7555321
  // Wraps SVG text
  function wrap (text, width) {
    text.each(function () {
      var text = d3.select(this)
      var words = text.text().split(/\s+/).reverse()
      var word
      var line = []
      var lineNumber = 0
      var lineHeight = 1.4 // ems
      var y = text.attr('y')
      var x = text.attr('x')
      var dy = parseFloat(text.attr('dy'))
      var tspan = text.text(null).append('tspan').attr('x', x).attr('y', y).attr('dy', dy + 'em')

      while (words.length > 0) {
        word = words.pop()
        line.push(word)
        tspan.text(line.join(' '))
        if (tspan.node().getComputedTextLength() > width) {
          line.pop()
          tspan.text(line.join(' '))
          line = [word]
          tspan = text.append('tspan').attr('x', x).attr('y', y).attr('dy', ++lineNumber * lineHeight + dy + 'em').text(word)
        }
      }
    })
  }// wrap
}// spiderPlot
