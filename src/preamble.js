/*
 * The preamble contains all globally relevant imports and requirements
 */

import './css/text.css'
import './css/colors.css'
import 'typeface-quicksand'
import './plugins/vuetify'

import Vue from 'vue'

/* istanbul ignore next */
Vue.config.productionTip = false
